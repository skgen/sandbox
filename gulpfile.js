var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('nodeWatcher', function () {
    nodemon({
        script: 'nodeFun/main.js'
    })
});

gulp.task('default', ['nodeWatcher']);