<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<?php

$obj = [
    'val1' => 0,
    'val2' => 2
];

$arr = [5, 6, 7, 8, 9];

$str = 'test str';
$nbr = 2;

$data = [
    $obj,
    $arr,
    $str,
    $nbr
];


function backToFrontDataSwitcher($data = null, $name = null, $log = false)
{
    if (isset($data) && isset($name)) {
        $print = $logger = '<script type="text/javascript">';
        $print .= 'var ' . $name . ' = ' . json_encode($data) . ';';
        $print .= '</script>';
        $logger .= 'console.log(' . $name . ');';
        $logger .= '</script>';
        print $print;
        $log ? print $logger : null;
    }
}

backToFrontDataSwitcher($data, 'data', true);

?>

<script src="main.js"></script>
</body>
</html>