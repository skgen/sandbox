/**
 * Created by Sko on 20/05/2017.
 */
var NodeServer = function () {
};

NodeServer.prototype.init = function (name) {
    if(name == undefined) {
       name = 'Generic'
    }
    this.express = require('express');
    this.app = this.express();
    this.server = require('http').createServer(this.app);
    this.config = {};
    this.debug = true;
    this.name = name;
    this.extends = {};
};

NodeServer.prototype.setupConfiguration = function (isLocal, ip, port) {
    if (isLocal) {
        this.config.IP = '127.0.0.1';
        this.config.localIP = '0.0.0.1';
        this.config.type = 'local';
    } else {
        this.config.IP = ip;
        this.config.localIP = ip;
        this.config.type = 'hosted';
    }
    this.config.port = port;
};

NodeServer.prototype.start = function () {
    this.server.listen(this.config.port, this.config.IP);
    this.log(this.name + ' :-> Node Server started on : ' + this.config.localIP + ':' + this.config.port + ' (' + this.config.type + ')');
};

NodeServer.prototype.log = function (toBeLogged) {
    if (this.debug) {
        console.log(toBeLogged);
    }
};

module.exports = NodeServer;