/**
 * Created by LCHAPUIS on 28/02/2017.
 */

var comboKeyPressListener = (function () {

    var ComboKeyPressListener = {

        init: function () {
            this.keyCodes = {
                valueToKey: {
                    'backspace': '8',
                    'tab': '9',
                    'enter': '13',
                    'shift': '16',
                    'ctrl': '17',
                    'alt': '18',
                    '0': '48',
                    '1': '49',
                    '2': '50',
                    '3': '51',
                    '4': '52',
                    '5': '53',
                    '6': '54',
                    '7': '55',
                    '8': '56',
                    '9': '57',
                    'a': '65',
                    'b': '66',
                    'c': '67',
                    'd': '68',
                    'e': '69',
                    'f': '70',
                    'g': '71',
                    'h': '72',
                    'i': '73',
                    'j': '74',
                    'k': '75',
                    'l': '76',
                    'm': '77',
                    'n': '78',
                    'o': '79',
                    'p': '80',
                    'q': '81',
                    'r': '82',
                    's': '83',
                    't': '84',
                    'u': '85',
                    'v': '86',
                    'w': '87',
                    'x': '88',
                    'y': '89',
                    'z': '90'
                },
                keyToValue: {
                    '8': 'backspace',
                    '9': 'tab',
                    '13': 'enter',
                    '16': 'shift',
                    '17': 'ctrl',
                    '18': 'alt',
                    '48': '0',
                    '49': '1',
                    '50': '2',
                    '51': '3',
                    '52': '4',
                    '53': '5',
                    '54': '6',
                    '55': '7',
                    '56': '8',
                    '57': '9',
                    '65': 'a',
                    '66': 'b',
                    '67': 'c',
                    '68': 'd',
                    '69': 'e',
                    '70': 'f',
                    '71': 'g',
                    '72': 'h',
                    '73': 'i',
                    '74': 'j',
                    '75': 'k',
                    '76': 'l',
                    '77': 'm',
                    '78': 'n',
                    '79': 'o',
                    '80': 'p',
                    '81': 'q',
                    '82': 'r',
                    '83': 's',
                    '84': 't',
                    '85': 'u',
                    '86': 'v',
                    '87': 'w',
                    '88': 'x',
                    '89': 'y',
                    '90': 'z'
                }
            };

            this.isFirstKeyDown = false;
            this.whichFirstKeyIsDown = null;
            this.isSecondKeyDown = false;
            this.whichSecondKeyisDown = null;

            this.debug = false;

            this.comboList = [];

            this.listenToKeyUp();
            this.listenToKeyDown();
        },
        listenToKeyUp: function () {
            document.addEventListener('keyup', (function (e) {
                if (this.isFirstKeyDown && this.keyCodes.keyToValue[e.keyCode] == this.whichFirstKeyIsDown || this.isSecondKeyDown && this.keyCodes.keyToValue[e.keyCode] == this.whichSecondKeyisDown) {

                    this.log('La touche relevée est ' + this.keyCodes.keyToValue[e.keyCode]);
                    this.log('Reset du combo');

                    if(this.keyCodes.keyToValue[e.keyCode] == this.whichFirstKeyIsDown) {
                        if(this.isSecondKeyDown != false) {
                            this.isFirstKeyDown = true;
                            this.whichFirstKeyIsDown = this.whichSecondKeyisDown;
                            this.isSecondKeyDown = false;
                            this.whichSecondKeyisDown = null;
                        } else {
                            this.whichFirstKeyIsDown = null;
                            this.isFirstKeyDown = false;
                        }
                    } else if (this.keyCodes.keyToValue[e.keyCode] == this.whichSecondKeyisDown){
                        this.isSecondKeyDown = false;
                        this.whichSecondKeyisDown = null;
                    }
                }
            }).bind(this));
        },
        listenToKeyDown: function() {
            document.addEventListener('keydown', (function (e) {
                // Si on a une touche pressée, et que ce n'est pas la premiere = combo de touche
                if (this.isFirstKeyDown && this.whichFirstKeyIsDown != this.keyCodes.keyToValue[e.keyCode] && this.whichSecondKeyisDown != this.keyCodes.keyToValue[e.keyCode]) {
                    this.isSecondKeyDown = true;
                    this.whichSecondKeyisDown = this.keyCodes.keyToValue[e.keyCode];
                    this.log('La touche pressé est ' + this.keyCodes.keyToValue[e.keyCode]);
                    this.log('You pressed ' + this.whichFirstKeyIsDown + ' + ' + this.whichSecondKeyisDown);

                    var i, j;
                    for(i = 0; i < this.comboList.length; i++) {
                        if(this.comboList[i].fKey == this.whichFirstKeyIsDown && this.comboList[i].sKey == this.whichSecondKeyisDown) {
                            for(j = 0; j < this.comboList[i].callbacks.length; j++) {
                                this.comboList[i].callbacks[j]();
                            }
                            return;
                        }
                    }
                    /* We have a combo pressed */

                } else if (this.whichFirstKeyIsDown != this.keyCodes.keyToValue[e.keyCode] && this.whichSecondKeyisDown != this.keyCodes.keyToValue[e.keyCode]) {
                    this.isFirstKeyDown = true;
                    this.whichFirstKeyIsDown = this.keyCodes.keyToValue[e.keyCode];
                    this.log('La touche pressé est ' + this.keyCodes.keyToValue[e.keyCode]);
                    this.log('keypressed');
                }
            }).bind(this));
        },

        addCombo : function(firstKey, secondKey, callback) {

            var i, exist = false;

            for(i = 0; i < this.comboList.length; i++) {
                if(this.comboList[i].fKey == firstKey && this.comboList[i].sKey == secondKey) {
                    // it exists
                    exist = true;
                    this.comboList[i].callbacks.push(callback);
                    return;
                }
            }

            if(!exist) {
                this.comboList.push({
                    'fKey': firstKey,
                    'sKey': secondKey,
                    'callbacks': [
                        callback
                    ]
                });
            }

            this.log(this.comboList);
        },

        log : function(toBeLogged) {
            if(this.debug) {
                console.log(toBeLogged);
            }
        }
    };

    ComboKeyPressListener.init();

    return ComboKeyPressListener;
}());

comboKeyPressListener.addCombo('k', 'l', function() {
    alert('yolo');
});

comboKeyPressListener.addCombo('k', 'l', function() {
    alert('salut');
});